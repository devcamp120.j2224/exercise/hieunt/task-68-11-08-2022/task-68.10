package com.pizza_db.userordercrud.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.pizza_db.userordercrud.model.*;
import com.pizza_db.userordercrud.repository.*;


@RequestMapping("/user")
@RestController
@CrossOrigin(value = "*" , maxAge = -1)
public class UserController {
    @Autowired
    IUserRepository iUserRepository;
    
    @GetMapping("/all")
    public ResponseEntity<List<CUser>> getAllUsers() {
        try {
            List<CUser> listUser = new ArrayList<CUser>();
            iUserRepository.findAll().forEach(listUser::add);
            return new ResponseEntity<List<CUser>>(listUser, HttpStatus.OK);
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<CUser> getUserById(@PathVariable long id) {
        Optional<CUser> foundUser = iUserRepository.findById(id);
        if(foundUser.isPresent()) {
            return new ResponseEntity<>(foundUser.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createUser(@RequestBody CUser paramUser) {
        try {
            CUser newUser = new CUser();
            newUser.setFullName(paramUser.getFullName());
            newUser.setEmail(paramUser.getEmail());
            newUser.setPhone(paramUser.getPhone());
            newUser.setAddress(paramUser.getAddress());
            return new ResponseEntity<>(iUserRepository.save(newUser), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create User: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable long id, @RequestBody CUser paramUser) {
        Optional<CUser> existedUserData = iUserRepository.findById(id);
        if(existedUserData.isPresent()) {
            CUser existedUser = existedUserData.get();
            existedUser.setFullName(paramUser.getFullName());
            existedUser.setEmail(paramUser.getEmail());
            existedUser.setPhone(paramUser.getPhone());
            existedUser.setAddress(paramUser.getAddress());
            try {
                return ResponseEntity.ok(iUserRepository.save(existedUser));
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update User: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable long id) {
        Optional<CUser> existedUserData = iUserRepository.findById(id);
        if(existedUserData.isPresent()) {
            try {
                iUserRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Failed to Delete User: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
        }
    }

}
